# SPDX-FileCopyrightText: 2022 Endo Renberg
#
# SPDX-License-Identifier: Unlicense

import std/[asyncdispatch, math, sets, sequtils, unicode]
from std/bitops import countSetBits

type
  Key* = uint64

  Arity = range[3..4]

  BinaryHashes = tuple
    h0, h1, h2: uint32

  Filter*[T: uint8 | uint16] = object
    seed: uint64
    segmentLength: int
    segmentCount: int
    fingerprints*: seq[T]

func segmentLengthMask(filter: Filter): uint32 =
  filter.segmentLength.uint32.pred

func segmentCountLength(filter: Filter): int =
  filter.segmentCount * filter.segmentLength

proc initFilter[T](size: Natural): Filter[T] =
  const arity: Arity = 3
  let n = float size
  result.segmentLength =
    case arity
    of 3: 1 shl int(floor(ln(float size) / ln(3.33) + 2.25))
    of 4: 1 shl int(floor(ln(float size) / ln(2.91) - 0.5))

  let minArrayLength =
    case arity
    of 3:
      int (n * max(
        0.875 + 0.25 * max(1, ln(float(10^6)) / ln(n)),
        1.125))
    of 4:
      int (n * max(
        0.77 + 0.305 * max(1, ln(float(6 * (10^5))) / ln(n)),
        1.075))

  var initsegmentCount = (minArrayLength + result.segmentLength - 1) div
      result.segmentLength - (arity - 1)

  var arrayLength = (initsegmentCount + arity - 1) * result.segmentLength
  result.segmentCount = (minArrayLength + result.segmentLength - 1) div
    result.segmentLength
  if result.segmentCount <= arity.pred:
    result.segmentCount = 1
  else:
    result.segmentCount = result.segmentCount - arity.pred
  arrayLength = (result.segmentCount + arity - 1) * result.segmentLength
  result.fingerprints.setLen(arrayLength)
  #[
  let bs = int recommendedBlockSize(
    sizeOf(T) * max(minArrayLength, result.segmentLength))
  let roundedSize = pred(result.fingerprints.len + pred(bs) and not(pred(bs)))
  result.fingerprints.setLen(roundedSize)
  result.segmentCount = result.fingerprints.len div result.segmentLength
  ]#

const XOR_MAX_ITERATIONS* = 100

proc zeroMem[T](x: openArray[T]) =
  zeroMem(unsafeAddr x[0], x.len * sizeof(T))

proc murmur64(h: uint64): uint64 {.inline.} =
  result = h
  result = result xor result shr 33
  result = result * 0xff51afd7ed558ccd'u64
  result = result xor result shr 33
  result = result * 0xc4ceb9fe1a85ec53'u64
  result = result xor result shr 33

proc mix_split(key: uint64; seed: uint64): uint64 {.inline.} =
  return murmur64(key + seed)

proc fingerprint(hash: uint64): uint64 {.inline.} =
  hash xor (hash shr 32)

proc rng_splitmix64(seed: var uint64): uint64 {.inline.} =
  ## We need a decent random number generator.
  ## Returns random number, modifies the seed.
  seed = seed + 0x9E3779B97F4A7C15'u64
  var z = seed
  z = (z xor (z shr 30)) * 0xBF58476D1CE4E5B9'u64
  z = (z xor (z shr 27)) * 0x94D049BB133111EB'u64
  z xor (z shr 31)

{.emit: """
static uint64_t mulhi(uint64_t a, uint64_t b) {
  return ((__uint128_t)a * b) >> 64;
}
""".}

proc mulhi(a, b: uint64): uint64 {.importc.}

func hash_batch(filter: Filter; hash: uint64): BinaryHashes =
  result.h0 = uint32 mulhi(hash, filter.segmentCountLength.uint64)
  result.h1 = result.h0 + filter.segmentLength.uint32
  result.h2 = result.h1 + filter.segmentLength.uint32
  result.h1 = result.h1 xor uint32(hash shr 18) and filter.segmentLengthMask
  result.h2 = result.h2 xor uint32(hash) and filter.segmentLengthMask

func hash*(filter: Filter; index: int; hash: uint64): uint32 =
  var h = mulhi(hash, filter.segmentCountLength.uint64)
  h = h + uint64(index * filter.segmentLength.int)
  ##  keep the lower 36 bits
  var hh: uint64 = hash and ((1'u64 shl 36) - 1)
  ##  index 0: right shift by 36; index 1: right shift by 18; index 2: no shift
  h = h xor ((hh shr (36 - 18 * index)) and filter.segmentLengthMask)
  uint32 h

func contains*[T](filter: Filter[T]; key: uint64): bool =
  var
    hash = mix_split(key, filter.seed)
    f = T fingerprint(hash)
    hashes = hash_batch(filter, hash)
  f = f xor (
      filter.fingerprints[hashes.h0] xor
      filter.fingerprints[hashes.h1] xor
      filter.fingerprints[hashes.h2])
  f == 0

proc mod3(x: uint8): uint8 {.inline.} =
  return if x > 2: x - 3 else: x

##  construct the filter, returns true on success, false on failure.
##  most likely, a failure is due to too high a memory usage
##  size is the number of keys
##  The caller is responsable for calling binary_fuse8_allocate(size,filter)
##  before. The caller is responsible to ensure that there are not too many  duplicated
##  keys. The inner loop will run up to XOR_MAX_ITERATIONS times (default on
##  100), it should never fail, except if there are many duplicated keys. If it fails,
##  a return value of false is provided.
##
##
##  If there are many duplicated keys and you do not want to remove them, you can first
##  sort your input, the algorithm will then work adequately.

proc populate[T](filter: var Filter[T]; keys: openArray[Key]) =
  assert(filter.fingerprints.len > 0)
  var size = keys.len
  var rng_counter: uint64 = 0x726b2b9d438b9d4d'u64
  filter.seed = rng_splitmix64(rng_counter)
  let
    capacity = filter.fingerprints.len
  var
    reverseOrder = newSeq[uint64](size + 1)
    alone = newSeq[int](capacity)
    t2count = newSeq[uint8](capacity)
    reverseH = newseq[uint8](size)
    t2hash = newSeq[uint64](capacity)
    blockBits = 1'u32
  while 1'u32 shl blockBits < filter.segmentCount.uint32:
    inc(blockBits)
  var
    `block` = 1'u32 shl blockBits
    startPos = newSeq[int](1 shl blockBits)
    h012: array[5, uint32]

  reverseOrder[size] = 1
  var loop = 0
  while true:
    doAssert(
      loop < XOR_MAX_ITERATIONS,
      "Too many iterations. Are all your keys unique?")
    inc(loop)
    for i in 0 ..< `block`:
      ##  important : i * size would overflow as a 32-bit number in some
      ##  cases.
      startPos[i] = int((i.uint64 * size.uint64) shr blockBits)

    var maskblock: uint64 = `block` - 1
    for i in 0 ..< size:
      var hash: uint64 = murmur64(keys[i] + filter.seed)
      var segment_index: uint64 = hash shr (64 - blockBits)
      while reverseOrder[startPos[segment_index]] != 0:
        inc(segment_index)
        segment_index = segment_index and maskblock
      reverseOrder[startPos[segment_index]] = hash
      inc(startPos[segment_index])
      doAssert(startPos[segment_index] < reverseOrder.len)
    var error, duplicates: int
    for i in 0 ..< size:
      var hash = reverseOrder[i]
      var h0 = hash(filter, 0, hash)
      inc(t2count[h0], 4)
      t2hash[h0] = t2hash[h0] xor hash
      var h1 = hash(filter, 1, hash)
      inc(t2count[h1], 4)
      t2count[h1] = t2count[h1] xor 1
      t2hash[h1] = t2hash[h1] xor hash
      var h2 = hash(filter, 2, hash)
      inc(t2count[h2], 4)
      t2hash[h2] = t2hash[h2] xor hash
      t2count[h2] = t2count[h2] xor 2
      if (t2hash[h0] and t2hash[h1] and t2hash[h2]) == 0:
        if ((t2hash[h0] == 0) and (t2count[h0] == 8)) or
            ((t2hash[h1] == 0) and (t2count[h1] == 8)) or
            ((t2hash[h2] == 0) and (t2count[h2] == 8)):
          inc(duplicates)
          dec(t2count[h0], 4)
          t2hash[h0] = t2hash[h0] xor hash
          dec(t2count[h1], 4)
          t2count[h1] = t2count[h1] xor 1
          t2hash[h1] = t2hash[h1] xor hash
          dec(t2count[h2], 4)
          t2count[h2] = t2count[h2] xor 2
          t2hash[h2] = t2hash[h2] xor hash
      error = if (t2count[h0] < 4): 1 else: error
      error = if (t2count[h1] < 4): 1 else: error
      error = if (t2count[h2] < 4): 1 else: error
    if error != 0:
      zeroMem(reverseOrder)
      zeroMem(t2count)
      zeroMem(t2hash)
      filter.seed = rng_splitmix64(rng_counter)
      inc(loop)
      continue
    var Qsize = 0
    ##  Add sets with one key to the queue.
    for i in 0 ..< capacity:
      alone[Qsize] = i
      inc(Qsize, if ((t2count[i] shr 2) == 1): 1 else: 0)
    var stacksize = 0
    while Qsize > 0:
      dec(Qsize)
      var index = alone[Qsize]
      if (t2count[index] shr 2) == 1:
        var hash: uint64 = t2hash[index]
        ## h012[0] = hash(filter, 0, hash, filter);
        h012[1] = hash(filter, 1, hash)
        h012[2] = hash(filter, 2, hash)
        h012[3] = hash(filter, 0, hash)
        ##  == h012[0];
        h012[4] = h012[1]
        var found: uint8 = t2count[index] and 3
        reverseH[stacksize] = found
        reverseOrder[stacksize] = hash
        inc(stacksize)
        var other_index1 = h012[found + 1]
        alone[Qsize] = int other_index1
        inc(Qsize, (if (t2count[other_index1] shr 2) == 2: 1 else: 0))
        dec(t2count[other_index1], 4)
        t2count[other_index1] = t2count[other_index1] xor
            mod3(found + 1)
        t2hash[other_index1] = t2hash[other_index1] xor hash
        var other_index2: uint32 = h012[found + 2]
        alone[Qsize] = int other_index2
        inc(Qsize, (if (t2count[other_index2] shr 2) == 2: 1 else: 0))
        dec(t2count[other_index2], 4)
        t2count[other_index2] = t2count[other_index2] xor
            mod3(found + 2)
        t2hash[other_index2] = t2hash[other_index2] xor hash
    if stacksize + duplicates == size:
      ##  success
      size = stacksize
      break
    zeroMem(reverseOrder)
    zeroMem(t2count)
    zeroMem(t2hash)
    filter.seed = rng_splitmix64(rng_counter)

  for i in countdown(pred size, 0):
    ##  the hash of the key we insert next
    var hash = reverseOrder[i]
    var xor2 = T fingerprint(hash)
    var found: uint8 = reverseH[i]
    h012[0] = hash(filter, 0, hash)
    h012[1] = hash(filter, 1, hash)
    h012[2] = hash(filter, 2, hash)
    h012[3] = h012[0]
    h012[4] = h012[1]
    filter.fingerprints[h012[found]] =
        xor2 xor
        filter.fingerprints[h012[found + 1]] xor
        filter.fingerprints[h012[found + 2]]

proc toFilter*[T](keys: openArray[uint64]): Filter[T] =
  result = initFilter[T](keys.len)
  populate(result, keys)

func saturation*[T](filter: Filter[T]): float =
  ## Return the saturation of `filter`.
  ## As the result approaches 1.0 the false positive
  ## rate of the filter increases.
  var total: int
  for word in filter.fingerprints: inc(total, countSetBits(word))
  (1.0 / float(filter.fingerprints.len * sizeOf(T) * 8)) * float(total)

func `$`*[T](filter: Filter[T]): string =
  ## Convert `filter` to a visual representation.
  const baseRune = 0x2800
  result = newString(filter.fingerprints.len * 3)
  var pos = 0
  for b in filter.fingerprints.items:
    when T is uint8:
      let r = (Rune)baseRune or b.int
      fastToUTF8Copy(r, result, pos, true)
    elif T is uint16:
      let
        h = (Rune)baseRune or int(b shr 8)
        l = (Rune)baseRune or int(b and 0xff)
      fastToUTF8Copy(h, result, pos, true)
      fastToUTF8Copy(l, result, pos, true)

