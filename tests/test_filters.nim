# SPDX-FileCopyrightText: 2022 Endo Renberg
# SPDX-License-Identifier: Unlicense

import std/[math, random, sequtils, unittest]
import binary_fuse_filters

func lowerBound(falsePositiveRate: float): float =
  ## Information-theoretic lower bound of filter efficiency.
  log2(1 / falsePositiveRate)

var rng = initRand()

proc randomKeys(n: Natural): seq[Key] =
  result.setLen(n)
  for key in result.mitems: key = next(rng)

proc basicTest(T: typedesc) =
  suite $T:
    for e in 2..6:
      let n = 10^e
      test $n:
        let
          keys = randomKeys(n)
          filter = toFilter[T](keys)
        for key in keys:
          check filter.contains(key)
        var trials, randomMatches: int
        while randomMatches < ((n div e) div 2):
          randomMatches.inc countIt(randomKeys(n), filter.contains(it))
          trials.inc n
        let
          falsePositiveRate = randomMatches.float / trials.float
          bitsPerKey = float(filter.fingerprints.len * sizeOf(T)) / float(n)
        check(bitsPerKey < 4)
        checkpoint("falsePositiveRate: " & $falsePositiveRate)
        check(falsePositiveRate < 0.005)

suite "Filter":
  basicTest uint8
  basicTest uint16
