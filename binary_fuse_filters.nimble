# Package

version       = "0.1.0"
author        = "Emery Hemingway"
description   = "Binary fuse filters"
license       = "Unlicense"
srcDir        = "src"


# Dependencies

requires "nim >= 1.6.6"
